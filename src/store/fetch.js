const baseURL = 'https://apiplug.sger.com.br/v1/aol';

export function _fetch (path) {
  return fetch({
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  })
  .then((response) => response.json());
}

export function fetchPosts (type) {
  return _fetch(`${type}`);
}

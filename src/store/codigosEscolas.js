const codigosEsolas = [
    {code:'0009', name: 'safa'                          ,tokenBanco:''},                   
    {code:'0010', name: 'reinoencantado'                ,tokenBanco:''},         
    {code:'0011', name: 'cecap'                         ,tokenBanco:''},                  
    {code:'0013', name: 'guiness'                       ,tokenBanco:''},                
    {code:'0019', name: 'security'                      ,tokenBanco:''},               
    {code:'0020', name: 'imaculada'                     ,tokenBanco:''},              
    {code:'0023', name: 'escolaalencar'                 ,tokenBanco:''},          
    {code:'0026', name: 'infolog'                       ,tokenBanco:''},                
    {code:'0036', name: 'lasalle'                       ,tokenBanco:''},                
    {code:'0039', name: 'jk'                            ,tokenBanco:''},                     
    {code:'0059', name: 'assefe'                        ,tokenBanco:''},                 
    {code:'0064', name: 'pax'                           ,tokenBanco:''},                    
    {code:'0069', name: 'atual'                         ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjUwfQ.eYXIeH_Q2m_AKqc53K8c1UkfCzs4UoeqPVCJ-n0qBhs'},                  
    {code:'0072', name: 'anima'                         ,tokenBanco:''},                  
    {code:'0073', name: 'mse'                           ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjQ5Nn0.ciZ5Z-OGCCVyrpdln_xVDM_TRkXRvcULGFb8ICuhUGQ'},                    
    {code:'0078', name: 'lasallece'                     ,tokenBanco:''},              
    {code:'0081', name: 'sigma'                         ,tokenBanco:''},                  
    {code:'0082', name: 'esmadf'                        ,tokenBanco:''},                 
    {code:'0126', name: 'eeb'                           ,tokenBanco:''},                    
    {code:'0131', name: 'espacocriativo'                ,tokenBanco:''},         
    {code:'0149', name: 'iese'                          ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjc3fQ.1MzJs_9rADHM8WG77Qmxt5icXO3tRDVnJDSDUbvPSvM'},                   
    {code:'0223', name: 'geracaodigital'                ,tokenBanco:''},         
    {code:'0241', name: 'domcesar'                      ,tokenBanco:''},               
    {code:'0242', name: 'perpetuosocorro'               ,tokenBanco:''},        
    {code:'0243', name: 'sigmal2'                       ,tokenBanco:''},
    {code:'0246', name: 'csrdf'                         ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjYxNH0.oKZCUx1d2rifc7bJuFmpHdbIcHzAoP6R344ck-AeeRc'},
    {code:'0249', name: 'ineisigma'                     ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjY4MX0.6nNaOZM4z-R01SwuNHOXvV9L1tK5IjDG99zadTkdGeE'},
    {code:'0256', name: 'nilsef'                        ,tokenBanco:''},
    {code:'0257', name: 'ceneb'                         ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjM1OX0.u48JXC2qZZKzdOJhYDeKWX7vdq_VjkBVIisSfqmfb9M'},
    {code:'0258', name: 'colegiomaanaim'                ,tokenBanco:''},
    {code:'0259', name: 'colegioceplen'                 ,tokenBanco:''},
    {code:'0263', name: 'colegiomontecalvario'          ,tokenBanco:''},
    {code:'0264', name: 'colegiodomelizeu'              ,tokenBanco:''},   
    {code:'0269', name: 'escolasantaedwiges'            ,tokenBanco:''},
    {code:'0270', name: 'academiaceneb'                 ,tokenBanco:''},     
    {code:'0271', name: 'cenebkids'                     ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjM5Mn0.iMXqIdc9Y8A-oTVTmy1Q42559hEP82Ui_rN4SnOkjEo'},          
    {code:'0272', name: 'colegioaraberi'                ,tokenBanco:''},
    {code:'0273', name: 'sigmaac'                       ,tokenBanco:''},         
    {code:'0274', name: 'cebnsa'                        ,tokenBanco:''},
    {code:'0275', name: 'cef09'                         ,tokenBanco:''},
    {code:'0276', name: 'colegiosjt'                    ,tokenBanco:''},
    {code:'0277', name: 'valledefiladelfia'             ,tokenBanco:''},
    {code:'0280', name: 'celumni'                       ,tokenBanco:''},
    {code:'0281', name: 'colegioesplanada'              ,tokenBanco:''},
    {code:'0283', name: 'mr'                            ,tokenBanco:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvSWQiOjcxMX0.wezCjnSdn_1eau4v6Ep44kqy43CTOwfSn6F6mNScO5c'},
    {code:'0284', name: 'santavirginia'                 ,tokenBanco:''},
];

export const getCodigo = (escola) => {
    let verifica = codigosEsolas.filter((x) => x.name === escola)
    if (verifica.length === 0) {
        return null;
    }
    return verifica[0].code;
};

export const getTokenEscola = (escola) => {
    let verifica = codigosEsolas.filter((x) => x.name === escola)
    if (verifica.length === 0) {
        return null;
    }
    return verifica[0].tokenBanco;
};
